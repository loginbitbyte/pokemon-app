import os
import json

with open('pokemonImages.json') as data_file:
	data = json.load(data_file)

for img in data.iterkeys():
	url = "https://img.pokemondb.net/sprites/black-white/normal/"+img.lower()+".png"
	sh = "wget -O {0} {1}".format("img2/"+img+".jpg", url)
	os.system(sh)